using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WeatherGenerator
{
    public static GameObject[] GenerateWeather(MeshData meshData, int levelOfDetail, TerrainType[] regions)
    {
        int verts = meshData.vertices.Length;
        GameObject[] weatherArray = new GameObject[verts];

        if(levelOfDetail > 0)
        {
            weatherArray = null;
            return weatherArray;
        }

        for (int v=0; v < verts; v++)
        {
            float rand = Random.Range(0, 1);
            Vector3 vertex = meshData.vertices[v];
            for (int u=0; u < regions.Length; u++)
            {
                if (rand < regions[u].weatherChance)
                {
                    regions[u].weatherType.transform.position = new Vector3(vertex.x, 250, vertex.z);
                    weatherArray[v] = regions[u].weatherType;
                }
                else weatherArray[v] = null;
            }
        }

        return weatherArray;
    }
}
