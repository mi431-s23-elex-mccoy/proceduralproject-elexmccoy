using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public enum DrawMode { NoiseMap, ColorMap, Mesh };
    public DrawMode drawMode;

    const int mapChunkSize = 241;
    [Range(0,6)]
    public int levelOfDetail;
    public float noiseScale;

    public int octaves;
    [Range(0,1)]
    public float persistence;
    public float lacunarity;

    public float meshHeightMultiplier;
    public AnimationCurve meshHeightCurve;

    public int seed;
    public Vector2 offset; 

    public bool autoUpdate;

    public TerrainType[] regions;

    public bool useGradient = false;
    public Gradient colorGradient;
    private Color[] clut;

    public bool useWeather = false;
    [Range(10,30)]
    public int weatherCap;
    public int weatherCount = 0;
    [Range(2400,3000)]
    public int weatherDistance = 25;

    private bool weatherSpawned = false;
    private List<GameObject> allWeathers;
    private float weathRand;
    private int lastWeather = 0;

    public void GenerateMap()
    {
        float[,] noiseMap = Noise.GenerateNoiseMap(mapChunkSize, mapChunkSize, seed,
            noiseScale, octaves, persistence, lacunarity, offset);

        Color[] colorMap = new Color[mapChunkSize * mapChunkSize];
        for (int y = 0; y < mapChunkSize; y++)
        {
            for (int x = 0; x < mapChunkSize; x++)
            {
                //weathRand = Random.Range(0, 1);
                float currentHeight = noiseMap[x, y];
                if (!useGradient)
                {
                    for (int i = 0; i < regions.Length; i++)
                    {
                        if (currentHeight <= regions[i].height)
                        {
                            colorMap[y * mapChunkSize + x] = regions[i].color;
                            break;
                        }
                    }
                }
                else
                {
                    clut = new Color[mapChunkSize];
                    for (int j = 0; j < mapChunkSize; j++)
                    {
                        clut[j] = colorGradient.Evaluate(currentHeight);
                        colorMap[y * mapChunkSize + x] = clut[j];
                    }
                }

                if (useWeather && (lastWeather <= 0))
                {
                    weatherSpawned = false;
                    if (drawMode != DrawMode.Mesh || levelOfDetail > 0 ||
                        weatherCount >= weatherCap || weatherSpawned) return;
                    for (int u = regions.Length - 1; u >= 0; u--)
                    {
                        if (currentHeight >= regions[u].height &&
                            regions[u].weatherType != null)
                        {
                            weathRand = Random.Range(0f,1f);
                            Debug.Log(weathRand);
                            if (weathRand < regions[u].weatherChance)
                            {
                                GameObject weather = regions[u].weatherType;
                                GameObject currentWeather = Instantiate<GameObject>(weather);
                                currentWeather.transform.position = new Vector3(
                                    10*x - (mapChunkSize*5), 250,
                                    10*y - (mapChunkSize*5));
                                currentWeather.GetComponent<ParticleSystem>().Play();

                                weatherCount++;
                                lastWeather = weatherDistance + 1;
                                weatherSpawned = true;
                                allWeathers.Add(currentWeather);
                            }
                        }
                        if (weatherSpawned) break;
                    }
                }
                lastWeather--;
            }
        }

        MapDisplay display = FindObjectOfType<MapDisplay>();
        if (drawMode == DrawMode.NoiseMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(noiseMap));
        }
        else if (drawMode == DrawMode.ColorMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromColorMap(colorMap, mapChunkSize, mapChunkSize));
        }
        else if (drawMode == DrawMode.Mesh)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(noiseMap,
                meshHeightMultiplier, meshHeightCurve, levelOfDetail),
                TextureGenerator.TextureFromColorMap(colorMap, mapChunkSize, mapChunkSize));
        }
    }

    void OnValidate()
    {
        if (lacunarity < 1) lacunarity = 1;
        if (octaves < 0) octaves = 0;
        if (useWeather == false)
        {
            weatherCount = 0;
            foreach (GameObject w in allWeathers)
            {
                UnityEditor.EditorApplication.delayCall += () =>
                {
                    DestroyImmediate(w);
                };
            }
            allWeathers.Clear();
        }
    }
}

[System.Serializable]
public struct TerrainType
{
    public string name;
    public float height;
    public Color color;
    [Range(0,1)]
    public float weatherChance;
    public GameObject weatherType;
}